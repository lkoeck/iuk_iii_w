package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Date;
import models.Userdate;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.UserdateService;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class UserdateController extends Controller {

    private final UserdateService userdateService;

    @Inject
    public UserdateController(UserdateService userdateService) {
        this.userdateService = userdateService;
    }

    /**
     * Request to get a userdate with a given id.
     * @param  q long
     * @return request to DateService to get a specific date.
     */
    public CompletionStage<Result> event(String q) {
        return userdateService.get().thenApplyAsync(userdate -> ok(Json.toJson(userdate)));
    }

    /**
     * Request to get a date with a given id.
     * @param  id long
     * @return request to DateService to get a specific date.
     */
    public CompletionStage<Result> get(long id) {
        return userdateService.get(id).thenApplyAsync(userdate -> ok(Json.toJson(userdate)));
    }

    /**
     * Request to get all userdates with a given date_id
     * @param  q long
     * @return request to DateService to get a specific date.
     */
    public CompletionStage<Result> getUsers(long id){
        return userdateService.getUsers(id).thenApplyAsync(userdate -> ok(Json.toJson(userdate)));
    }

    //Bruchan ma des?? copy paste fehler??
    /**
     * Request to get a date with a given id.
     * @param  user_id long
     * @return request to DateService to get a specific date.
    public CompletionStage<Result> getUser(long user_id) {
        return userdateService.get(user_id).thenApplyAsync(userdate -> ok(Json.toJson(userdate)));
    }


     */

    /**
     * Request to add a userdate
     * @param  request http.Request
     * @return request to DateService to post a new date.
     */
    public CompletionStage<Result> add(Http.Request request) {
        JsonNode jsonUserdate = request.body().asJson();
        Userdate newUserdate = Json.fromJson(jsonUserdate, Userdate.class);
        return userdateService.add(newUserdate).thenApplyAsync(userdate -> ok(Json.toJson(userdate)));
    }

    /**
     * Request to update a userdate
     * @param  id long, request http.Resquest
     * @return request to DateService to update a specific userdate.
     */
    public CompletionStage<Result> update(Http.Request request, long id) {
        JsonNode jsonUserdate = request.body().asJson();
        Userdate updatedUserdate = Json.fromJson(jsonUserdate, Userdate.class);
        updatedUserdate.setId(id);
        return userdateService.update(updatedUserdate).thenApplyAsync(userdate -> ok(Json.toJson(userdate)));
    }

    /**
     * Request to delet a userdate with a given id
     * @param  id long
     * @return request to DateService to delet a specific date.
     */
    public CompletionStage<Result> delete(long id) {
        return userdateService.delete(id).thenApplyAsync(deleted -> deleted ? ok() : internalServerError());
    }
}
