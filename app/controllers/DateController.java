package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Date;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.DateService;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class DateController extends Controller {

    private final DateService dateService;

    @Inject
    public DateController(DateService dateService) {
        this.dateService = dateService;
    }

    /**
     * Request to get all dates.
     * @param q string
     * @return request to DateService to get all dates.
     */
    public CompletionStage<Result> dates(String q) {
        return dateService.get().thenApplyAsync(dateStream -> ok(Json.toJson(dateStream.collect(Collectors.toList()))));
    }

    /**
     * Request to get a date with a given id.
     * @param  id long
     * @return request to DateService to get a specific date.
     */
    public CompletionStage<Result> get(long id) {
        return dateService.get(id).thenApplyAsync(date -> ok(Json.toJson(date)));
    }

    /**
     * Request to add a date
     * @param  request http.Request
     * @return request to DateService to post a new date.
     */
    public CompletionStage<Result> add(Http.Request request) {
        JsonNode jsonDate = request.body().asJson();
        System.out.println(jsonDate);
        Date newDate = Json.fromJson(jsonDate, Date.class);
        return dateService.add(newDate).thenApplyAsync(date -> ok(Json.toJson(date)));
    }

    /**
     * Request to update a date
     * @param  id long, request http.Resquest
     * @return request to DateService to update a specific date.
     */
    public CompletionStage<Result> update(Http.Request request, long id) {
        JsonNode jsonDate = request.body().asJson();
       Date updatedDate = Json.fromJson(jsonDate, Date.class);
        updatedDate.setId(id);
        return dateService.update(updatedDate).thenApplyAsync(date -> ok(Json.toJson(date)));
    }

    /**
     * Request to delet a date with a given id
     * @param  id long
     * @return request to DateService to delet a specific date.
     */
    public CompletionStage<Result> delete(long id) {
        return dateService.delete(id).thenApplyAsync(deleted -> deleted ? ok() : internalServerError());
    }
}
