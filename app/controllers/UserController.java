package controllers;

import akka.http.javadsl.model.Query;
import com.fasterxml.jackson.databind.JsonNode;
import models.User;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.UserService;
import play.data.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class UserController extends Controller{
    private final UserService userService;

    @Inject
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Request to get all users.
     * @param q string
     * @return request to USerService to get all users.
     */
    public CompletionStage<Result> user(String q) {
        return userService.get().thenApplyAsync(userStream -> ok(Json.toJson(userStream.collect(Collectors.toList()))));
    }

    /**
     * Request to get a user with a given id.
     * @param  id long
     * @return request to UserService to get a specific user.
     */
    public CompletionStage<Result> get(long id) {
        return userService.get(id).thenApplyAsync(user -> ok(Json.toJson(user)));
    }

    /**
     * Request to login.
     * @param  request http.Request
     * @return request to UserService to get a specific user.
     */
    public CompletionStage<Result> login(Http.Request request) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        JsonNode jsonLogin = request.body().asJson();
        User userLogin = Json.fromJson(jsonLogin, User.class);
        String mail = userLogin.getMail();
        String password = userLogin.getPassword();
        byte[] salt = toString().getBytes(userLogin.getSalt());
        SecureRandom random = new SecureRandom();
        random.nextBytes(salt);
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(salt);
        byte[] hashedPassword = md.digest(userLogin.getPassword().getBytes(StandardCharsets.UTF_8));
        if (hashedPassword.toString() == password.toString()) {
//          return true;
            return (CompletionStage<Result>) redirect("/home");
        }
        return userService.login(userLogin.toString()).thenApplyAsync(user -> ok(Json.toJson(user)));
    }

    /**
     * Request to add a user
     * @param  request http.Request
     * @return request to UserService to post a new user.
     */
    public CompletionStage<Result> add(Http.Request request) throws NoSuchAlgorithmException {
        JsonNode jsonUser = request.body().asJson();
        User newUser = Json.fromJson(jsonUser, User.class);
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(salt);
        byte[] hashedPassword = md.digest(newUser.getPassword().getBytes(StandardCharsets.UTF_8));
        newUser.setPassword(hashedPassword.toString());
        newUser.setSalt(salt.toString());
        return userService.add(newUser).thenApplyAsync(user -> ok(Json.toJson(user)));
    }

    /**
     * Request to delet a user with a given id
     * @param  id long
     * @return request to UserService to delete a specific user.
     */
    public CompletionStage<Result> delete(long id) {
        return userService.delete(id).thenApplyAsync(deleted -> deleted ? ok() : internalServerError());
    }
}
