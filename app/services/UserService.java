package services;

import com.google.inject.ImplementedBy;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;
import models.User;

@ImplementedBy(DefaultUserService.class)
public interface UserService {
    /**
     * Return's list of all users.
     * @return list of all users
     */
    CompletionStage<Stream<User>> get();

    /**
     * Returns user with given identifier.
     * @param id user identifier
     * @return user with given identifier or {@code null}
     */
    CompletionStage<User> get(final Long id);

    /**
     * Returns user with given mail.
     * @param mail user mail
     * @return user with given mail or {@code null}
     */
    CompletionStage<User> login(final String mail);

    /**
     * Removes user with given identifier.
     * @param id user identifier
     * @return {@code true} on success  {@code false} on failure
     */
    CompletionStage<Boolean> delete(final Long id);

    /**
     * Updates user with given identifier.
     * @param updatedUser with updated fields
     * @return updated user
     */
    CompletionStage<User> update(final User updatedUser);

    /**
     * Adds the given user.
     * @param user to add
     * @return added user
     */
    CompletionStage<User> add(final User user);
}
