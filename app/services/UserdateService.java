package services;

import com.google.inject.ImplementedBy;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

import models.Date;
import models.Userdate;

@ImplementedBy(DefaultUserdateService.class)
public interface UserdateService {
    /**
     * Return's list of all userdates.
     * @return list of all userdates
     */
    CompletionStage<Stream<Userdate>> get();



    /**
     * Returns userdate with given identifier.
     * @param id userdate identifier
     * @return userdate with given identifier or {@code null}
     */
    CompletionStage<Userdate> get(final Long id);

    /**
     * Return's list of all userdates with given date-id
     * @param id date identifier
     * @return list of all userdates with given date-id
     */
    CompletionStage<Stream<Userdate>> getUsers(final Long id);

    /**
     * Removes userdate with given identifier.
     * @param id userdate identifier
     * @return {@code true} on success  {@code false} on failure
     */
    CompletionStage<Boolean> delete(final Long id);

    /**
     * Updates date with given identifier.
     * @param updatedUserdate with updated fields
     * @return updated date
     */
    CompletionStage<Userdate> update(final Userdate updatedUserdate);

    /**
     * Adds the given userdate.
     * @param userdate to add
     * @return added userdate
     */
    CompletionStage<Userdate> add(final Userdate userdate);


}
