package services;

import com.google.inject.ImplementedBy;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;
import models.Date;

@ImplementedBy(DefaultDateService.class)
public interface DateService {
    /**
     * Return's list of all dates.
     * @return list of all dates
     */
    CompletionStage<Stream<Date>> get();

    /**
     * Returns date with given identifier.
     * @param id date identifier
     * @return date with given identifier or {@code null}
     */
    CompletionStage<Date> get(final Long id);

    /**
     * Removes date with given identifier.
     * @param id date identifier
     * @return {@code true} on success  {@code false} on failure
     */
    CompletionStage<Boolean> delete(final Long id);

    /**
     * Updates date with given identifier.
     * @param updatedDate with updated fields
     * @return updated date
     */
    CompletionStage<Date> update(final Date updatedDate);

    /**
     * Adds the given date.
     * @param date to add
     * @return added date
     */
    CompletionStage<Date> add(final Date date);


}
