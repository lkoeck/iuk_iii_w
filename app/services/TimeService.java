package services;

import com.google.inject.ImplementedBy;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

import models.Time;

@ImplementedBy(DefaultTimeService.class)
public interface TimeService {

    /**
     * Return's list of all times.
     * @return list of all times
     */
    CompletionStage<Stream<Time>> get();

    /**
     * Returns time with given identifier.
     * @param id time identifier
     * @return time with given identifier or {@code null}
     */
    CompletionStage<Time> get(final Long id);

    /**
     * Removes time with given identifier.
     * @param id time identifier
     * @return {@code true} on success  {@code false} on failure
     */
    CompletionStage<Boolean> delete(final Long id);

    /**
     * Adds the given time.
     * @param time to add
     * @return added time
     */
    CompletionStage<Time> add(final Time time);
}
