package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity(name= "time")
public class Time {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //Datafields
    private long id;
    private long userdate_id;
    private long time;

    //Methods
    public long getId(){return id;}
    public void setId(Long id) { this.id = id; }

    public long getUserDate_id() { return userdate_id; }
    public void setUserDate_id(long userdate_id) { this.userdate_id = userdate_id; }

    public long getTime(){return time;}
    public void setTime(Long time) { this.time = time; }

}
