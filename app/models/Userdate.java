package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="userdate")
public class Userdate{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //Datafields
    private long id;
    private long user_id;
    private long date_id;
    private boolean fixed;

    //Methods

    public long getUser_id() {
        return user_id;
    }

    public long getDate_id() {
        return date_id;
    }

    public long getId() {
        return id;
    }

    public boolean isFixed() {
        return fixed;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public void setDate_id(long date_id) {
        this.date_id = date_id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFixed(boolean fixed) {
        this.fixed = fixed;
    }
}