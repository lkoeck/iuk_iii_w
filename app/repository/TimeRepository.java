package repository;

import javax.inject.Inject;
import play.db.jpa.JPAApi;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;
import models.Time;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class TimeRepository {
    private final JPAApi jpaApi;

    @Inject
    public TimeRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public CompletionStage<Time> add(Time time) {
        return supplyAsync(() -> wrap(em -> insert(em, time)));
    }

    public CompletionStage<Stream<Time>> list() {
        return supplyAsync(() -> wrap(this::list));
    }

    public CompletionStage<Time> find(Long id) {
        return supplyAsync(() -> wrap(em -> find(em, id)));
    }

    public CompletionStage<Boolean> delete(Long id) {
        return supplyAsync(() -> wrap(em -> delete(em, id)));
    }

    private <D> D wrap(Function<EntityManager, D> function) {
        return jpaApi.withTransaction(function);
    }

    //Methods

    /**
     * Inserts the given time
     *
     * @param time to insert
     * @return inserted time
     */
    private Time insert(EntityManager em, Time time) {
        em.persist(time);
        return time;
    }

    /**
     * Return's a list of all times
     *
     * @return list of all times
     */
    private Stream<Time> list(EntityManager em) {
        List<Time> times = em.createQuery("select t from time t", Time.class).getResultList();
        return times.stream();
    }

    /**
     * Returns date with given identifier
     * @param id date identifier
     * @return date
     */
    private Time find(EntityManager em, Long id) {
        return em.find(Time.class, id);
    }

    /**
     * Deletes dates with given identifier.
     * @param id dates identifier
     * @return {@code true} on success  {@code false} on failure
     */
    private Boolean delete(EntityManager em, Long id) {
        Time time = em.find(Time.class, id);
        if (null != time) {
            em.remove(time);
            return true;
        } else {
            return false;
        }
    }

}
