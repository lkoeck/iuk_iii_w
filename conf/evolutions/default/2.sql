# Users schema

# --- !Ups
create table users (
    id integer GENERATED ALWAYS AS IDENTITY,
    mail VARCHAR(50),
    firstname VARCHAR(50),
    name VARCHAR(50),
    password VARCHAR(64),
    salt VARCHAR(64),
    role boolean NOT NULL,
    PRIMARY KEY (id)
);

# --- !Downs
drop table users;