# Userdate schema

# --- !Ups
create table userdate (
    id integer GENERATED ALWAYS AS IDENTITY,
    user_id integer NOT NULL,
    date_id integer NOT NULL,
    fixed boolean,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ,
    FOREIGN KEY (date_id) REFERENCES date(id) ON DELETE CASCADE ,
    PRIMARY KEY (id)
);

# --- !Downs
drop table userdate;