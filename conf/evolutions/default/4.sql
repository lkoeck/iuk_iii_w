# Time schema

# --- !Ups
create table time (
    id integer GENERATED ALWAYS AS IDENTITY,
    userdate_id integer NOT NULL ,
    time float,
    FOREIGN KEY (userdate_id) REFERENCES userdate(id),
    PRIMARY KEY (id)
);

# --- !Downs
drop table time;