# Event schema

# --- !Ups
create table date (
    id integer GENERATED ALWAYS AS IDENTITY,
    date VARCHAR(50),
    title VARCHAR(50),
    place VARCHAR(50),
    nrPeople integer,
    description VARCHAR(600),
    PRIMARY KEY (id)
);


# --- !Downs
drop table date;


