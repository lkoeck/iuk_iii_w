import React from "react";
import "./index.css";
import {toast} from 'react-toastify';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    handleSubmit(event) {
        event.preventDefault();
        //Create JSON
        let body = {
            email: this.state.email,
            password: this.state.password
        }
        //validation
        //Post Data on success
        //console.log("success");
        let url  = '/api/users/login/'+ this.state.email;
        fetch(url, {
            method: 'post',
            body: JSON.stringify(body),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }

        }).then(response => {
            //response success / error
            if (response.status !== 200) {
                console.log(response.status);
                toast.error('Ups! Da ist wohl ein Fehler passiert', {
                    position: "bottom-center",
                });
            } else {
                toast('Eingeloggt', {
                    position: "bottom-center",
                });
                // set initial state
                this.setState({
                    email: "",
                    password: "",
                   });
                return response.json();
                }
            })
    }

    //Update state OnChange
    handleInputChange(event) {
        let target = event.target;
        let value = target.value;
        let name = target.name;
        //console.log(value);
        //console.log(name);

        this.setState({[name]: value});
    }

    render() {
        return (
            <section>
                <h1>Welcome to Timeplanning for Frastanzer!</h1>
                <h2>Login to Apply for Work</h2>
                <form className="input-form" id="form" onSubmit={this.handleSubmit}>
                    <div className="frame">
                        <h1>Login:</h1>
                        <label>Email:</label>
                        <input className="margin2" type="text" name="email" value={this.state.email}
                               onChange={this.handleInputChange}/><br/>

                        <label>Password:</label>
                        <input className="margin2" type="password" name="password" value={this.state.password}
                               onChange={this.handleInputChange}/><br/>
                        <input className="submit button margin" type="submit" value="Login"/>
                    </div>
                </form>
            </section>
        )
    }
}
export default Home;