import React from "react";
import "./GlobalStyles.css";
import FixedUsers from "./Application/FixedUsers";
import {Table, Button, Modal} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

class Einsatz extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            event: this.props.date,
            title: this.props.date.title,
            date: this.props.date.date,
            nrpeople: this.props.date.nrpeople,
            description: this.props.date.description,
            show: false,
        };
        this.toggleShow = this.toggleShow.bind(this);
    }

    toggleShow(){
        this.setState({show: !this.state.show});
    }

    render() {
        //console.log(this.state.event);
        return(
            <div className="date">
                <Table>
                    <tbody>
                    <tr>
                        <td>
                            <div className="date2">
                                <p>{this.state.title}</p>
                                <p>Datum: {this.state.date}</p>
                                <p>Benötigte Personen: {this.state.nrpeople}</p>
                            </div>
                        </td>
                        <td>
                            <p  className="date2">Fixierte Personen:</p>
                            <FixedUsers key={this.state.event.id} date={this.state.event}/>
                        </td>
                        <td>
                            <Button variant="outline-danger" onClick={this.toggleShow}>Infos ansehen</Button>
                        </td>
                        <td>
                            <Button  variant="outline-danger">Stunden eintragen</Button>
                        </td>
                    </tr>
                    </tbody>
                </Table>
                <Modal show={this.state.show}>
                    <Modal.Header closeButton>
                        <Modal.Title>Info's</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.state.description}</Modal.Body>
                    <Modal.Footer>
                        <Button variant="outline-danger" onClick={this.toggleShow}>Schliessen</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
export default Einsatz;