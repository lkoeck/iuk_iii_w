import React from "react";
import "../GlobalStyles.css";
import {withRouter} from 'react-router-dom';

import {Table} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Checkbox , Button} from '@material-ui/core';
import {toast} from "react-toastify";
import Tableelement from "./Tableelement";

class ApplicationsShow extends React.Component {

    constructor(props) {
        super(props);
        this.state ={
            events: [],
            users: [],
            applications: [],
            disabledButton: false,
            checked: true,
        };
        this.filterUsers = this.filterUsers.bind(this);
        this.filterApplications = this.filterApplications.bind(this);
        this.addEvent = this.addEvent.bind(this);
        this.addUser = this.addUser.bind(this);
        this.addApplications = this.addApplications.bind(this);
        this.fixUser = this.fixUser.bind(this);
    }

    //get Infos
    componentDidMount() {
        //get Events
        fetch('/api/events')
            .then(res => {
                return res.json();
            })
            .then(data => {
                this.setState({
                    events: data
                });
            });

        //get Users
        fetch('/api/users')
            .then(res => {
                return res.json();
            })
            .then(data => {
                this.setState({
                    users: this.filterUsers(data)
                });
            });

        //get Applications
        fetch('/api/applications')
            .then(res => {
                return res.json();
            })
            .then(data => {
                this.setState({
                    applications: data
                });
            });
    }

    //filters users
    //Administratoren werden herausgefiltert
    filterUsers(data){
        let users = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].role === false) {
                users.push(data[i])
            }
        }
        return users;
    }

    // filters Applications with given even id
    // returns array with events
    filterApplications(event_id){
        let filteredApplications = [];

        for(let k = 0; k < this.state.applications.length; k++){
            if(this.state.applications[k].date_id === parseInt(event_id)){
               let application = this.state.applications[k];
                console.log("app " + application);
                filteredApplications.push(application);
            }
        }
        return filteredApplications;
    }

    // adds a new Row with Event + Checkboxes to the table
    addEvent(event){
        //const addApplications = this.addApplications;
        return(
            <tr><th>{event.date} <br/> {event.title} <br/> {event.nrPeople}</th>
                {
                    this.state.users.length > 0 ?
                        this.state.users.map(function (user) {
                            return <td><Tableelement key={event.id} event={event} user={user.id}/></td>
                            //return addApplications(user, event.id);
                        })

                                //return <Tableelement key={event.id} event={event} user={user.id}/>
                        //})
                : null
                }
            </tr>
        );
    }

    //adds a new Column with the username to the table
    addUser(user){
        return(
                <th>{user.firstname} {user.name}</th>
        );
    }

    //adds Checkbox to the table
    addApplications(user, event_id ){
        let items = [];
        let filteredApplications = this.filterApplications( parseInt(event_id));

        for (let k = 0; k < filteredApplications.length; k++) {

            // eslint-disable-next-line
            if ( filteredApplications[k].user_id == user.id) {
                if(filteredApplications[k].fixed === true){
                    items.push( <td><Checkbox checked={true} disabled={true}/></td>);
                }
                else{
                    items.push( <td><Checkbox checked={true} disabled={true}/><Button variant="outlined" disabled={this.state.disabledButton} className='button2' onClick={() => this.fixUser(filteredApplications[k])}>Fixieren</Button></td>);
                }
                break;
            }
        }
        if(items.length === 0){
            items.push(<td><Checkbox disabled={true}/></td>);
        }
        return items;
    }

    //Update application to 'fixed = true'
    fixUser(fixdate){
            //console.log('fixieren');
        //Create Json
        let body = {
            id: fixdate.id,
            user_id: fixdate.user_id,
            date_id: fixdate.date_id,
            fixed: true
        };
        //put json
        fetch('/api/applications/'+fixdate.id, {
            method: 'put',
            body: JSON.stringify(body),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }

        }).then(response => {
            //response success / error
            if (response.status !== 200) {
                toast.error('Ups! Da ist wohl ein Fehler passiert', {
                    position: "bottom-center",
                });
            } else {
                this.setState({disabledButton: true});
                toast('Teilnehmer fixiert!', {
                    position: "bottom-center",
                });
                //this.props.history.push('/calendar');
                return response.json();
            }
        })

    }

    render() {
        return(
            <section>
                <form>
                    <Table>
                        <thead>
                        <tr>
                            <th></th>
                            {
                                this.state.users.length > 0 ?
                                    this.state.users.map(this.addUser):<th>Keine Benutzer verfügbar!</th>

                            }
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.events.length > 0 ?
                                this.state.events.map(this.addEvent):<th>Keine Events verfügbar!</th>
                        }
                        </tbody>
                    </Table>
                </form>
            </section>
         );
    }
}

export default withRouter(ApplicationsShow);