import React from "react";
import "./GlobalStyles.css";
import {withRouter} from 'react-router-dom';
import {toast} from 'react-toastify';

class Createuser extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            mail: "",
            firstname: "",
            name: "",
            password: "",
            role: false,
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.back = this.back.bind(this);
        this.validEmail = this.validEmail.bind(this);
    }

    handleSubmit(event){
        event.preventDefault();
        //Create JSON
        let body = {
            mail: this.state.mail,
            firstname: this.state.firstname,
            name: this.state.name,
            password: this.state.password,
            role: this.state.role
        }
        //validation: everything filled
        if(body.mail.length === 0 || body.firstname.length === 0 || body.name.length === 0 || body.password.length === 0){
            toast.warn("Alle Felder sind auszufüllen", {position: "bottom-center",});

        }
        else {
            //validation: valid email address
            if(this.validEmail() === false){
                toast.warn("Die Email-Adresse ist ungültig", {position: "bottom-center",});
            }
            else {
                //Post Data on success
                //console.log("success");
                fetch('/api/users', {
                    method: 'post',
                    body: JSON.stringify(body),
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }

                }).then(response => {
                    //response success / error
                    if (response.status !== 200) {
                        toast.error('Ups! Da ist wohl ein Fehler passiert', {
                            position: "bottom-center",
                        });
                    } else {
                        toast('User hinzugefügt', {
                            position: "bottom-center",
                        });
                        // set initial state
                        this.setState({
                            mail: "",
                            firstname: "",
                            name: "",
                            password: "",
                        });
                        //this.props.history.push('/calendar');
                        return response.json();
                    }
                })
            }
        }
    }

    //Email Validation: returns true if valid
    validEmail(){
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        if (pattern.test(this.state.mail)) {
            return true;
        }
        return false;
    }

    //Update state OnChange
    handleInputChange(event){
        let target = event.target;
        let value = target.value;
        let name = target.name;
        //console.log(value);
        //console.log(name);

        this.setState({[name]: value});
    }

    //Link back to Calendar-page
    back(){
        let path = "/";
        this.props.history.push(path);
    }


    render(){
        return(
            <section>
                <button className="margin button" onClick={this.back}>Zurück zu Home</button>
                <form className="input-form" id="form" onSubmit={this.handleSubmit}>
                    <h1>Neuen Benutzer erstellen</h1>
                    <div className="frame">
                        <label>Vorname:</label>
                        <input className="margin2" type="text" name="firstname" value={this.state.firstname} onChange={this.handleInputChange}/><br/>

                        <label>Nachname:</label>
                        <input className="margin2" type="text" name="name" value={this.state.name} onChange={this.handleInputChange}/><br/>

                        <label>E-Mail:</label>
                        <input className="margin2" type="text" name="mail" value={this.state.mail} onChange={this.handleInputChange}/><br/>

                        <label>Passwort:</label>
                        <input className="margin2" type="password" name="password" value={this.state.password} min="1" max="20" onChange={this.handleInputChange}/><br/>

                        <input className="submit button margin" type="submit" value="User hinzufügen"/>
                    </div>
                </form>
            </section>
        )
    }
}
export default withRouter(Createuser);